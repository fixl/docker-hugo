FROM alpine:3.12

# Inject hugo version
ARG HUGO_VERSION

# Environment
ENV GLIBC_VERSION=2.32-r0

RUN apk --update --no-cache add \
    make \
    bash \
    curl \
    git \
    python3 \
    py3-setuptools \
    unzip \
    wget \
    libstdc++

# Install glibc: This is required for HUGO-extended (including SASS) to work.
RUN wget -q -O /etc/apk/keys/sgerrand.rsa.pub https://alpine-pkgs.sgerrand.com/sgerrand.rsa.pub \
    &&  wget "https://github.com/sgerrand/alpine-pkg-glibc/releases/download/$GLIBC_VERSION/glibc-$GLIBC_VERSION.apk" \
    &&  apk --no-cache add "glibc-$GLIBC_VERSION.apk" \
    &&  rm "glibc-$GLIBC_VERSION.apk" \
    &&  wget "https://github.com/sgerrand/alpine-pkg-glibc/releases/download/$GLIBC_VERSION/glibc-bin-$GLIBC_VERSION.apk" \
    &&  apk --no-cache add "glibc-bin-$GLIBC_VERSION.apk" \
    &&  rm "glibc-bin-$GLIBC_VERSION.apk" \
    &&  wget "https://github.com/sgerrand/alpine-pkg-glibc/releases/download/$GLIBC_VERSION/glibc-i18n-$GLIBC_VERSION.apk" \
    &&  apk --no-cache add "glibc-i18n-$GLIBC_VERSION.apk" \
    &&  rm "glibc-i18n-$GLIBC_VERSION.apk" \
    # Install Hugo
    && mkdir -p /tmp/hugo \
    && cd /tmp/hugo \
    && wget -q -O hugo.tar.gz https://github.com/gohugoio/hugo/releases/download/v${HUGO_VERSION}/hugo_extended_${HUGO_VERSION}_Linux-64bit.tar.gz \
    && HUGO_CHECKSUM=$(curl -L  https://github.com/gohugoio/hugo/releases/download/v${HUGO_VERSION}/hugo_${HUGO_VERSION}_checksums.txt | grep hugo_extended_${HUGO_VERSION}_Linux-64bit.tar.gz | awk '{print $1}') \
    && if [ "${HUGO_CHECKSUM}" != "$(sha256sum hugo.tar.gz | awk '{print $1}')" ]; then echo "Wrong sha256sum of downloaded file!"; exit 1; fi \
    && tar zxvf hugo.tar.gz \
    && mv hugo /usr/local/bin/hugo \
    && rm -rf /tmp/*

COPY ./run.sh /run.sh

WORKDIR /hugo
CMD ["/run.sh"]

EXPOSE 1313
