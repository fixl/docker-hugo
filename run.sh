#!/usr/bin/env sh

HUGO=/usr/local/bin/hugo

# --watch=false makes hugo not serve
${HUGO} server --disableLiveReload --bind=0.0.0.0 --appendPort=false --baseUrl=${HUGO_BASE_URL}
