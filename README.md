# Hugo Docker Image

[![pipeline status](https://gitlab.com/fixl/docker-hugo/badges/master/pipeline.svg)](https://gitlab.com/fixl/docker-hugo/-/commits/master)
[![layers](https://images.microbadger.com/badges/image/fixl/hugo.svg)](https://gitlab.com/fixl/docker-hugo/-/pipelines)
[![version](https://images.microbadger.com/badges/version/fixl/hugo.svg)](https://gitlab.com/fixl/docker-hugo)
[![Docker Pulls](https://img.shields.io/docker/pulls/fixl/hugo)](https://hub.docker.com/repository/docker/fixl/hugo)

A Docker container that runs your [Hugo](https://gohugo.io/) website.

## Environment Variables

- `HUGO_BASE_URL`: The base url of your server e.g. `https://fixl.info`

## Usage

Add your content to a docker container based on the hugo image
```
FROM fixl/hugo

ADD your/site /hugo
```

Build your new image
```
docker build --pull --tag ${USER}/my-hugo-website .
```

Run your new image
```
docker run -d -p 1313:1313 -e HUGO_BASE_URL=https://<url>md5sum ${USER}/my-hugo-website
```

## Build the image

```
make build
```

## Check metadata

```bash
docker image inspect fixl/hugo --format '{{ json .Config.Labels }}'
```
